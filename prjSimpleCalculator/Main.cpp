//Keir Dvorachek
// Simple Calculator
#include <iostream>
#include <conio.h>

using namespace std;
// Prototyping the formulas before int main.
float Add(float num1, float num2);
float Subtract(float num1, float num2);
float Multiply(float num1, float num2);
bool Divide(float num1, float num2, float &answer);
float Pow(float num1, int num2);

int main()
{
	// Variables for the equations to come.
	float num1 = 0;
	float num2 = 0;
	float answer = 0;
	char input = 'y';

	// While loop to ask if the user wants to do more math.
	while (input == 'y')
	{
		// First cout giving the user instructions on what to do to operate the simple calculator.
		cout << "Please denote the mathematical operation you would like to use; \n";
		cout << "For addition type \"+\" \n";
		cout << "For subtraction type \"-\" \n";
		cout << "For multiplication type \"*\" \n";
		cout << "For division type \"/\" \n";
		cout << "For exponents type \"^\" \n";
		cin >> input;
		cout << "Please enter the first number of your equation.\n";
		cin >> num1;
		cout << "Please enter the second number of your equation.\n";
		cin >> num2;
		
		// If statement leading to proper formulas for the operations that the use selects.
		if (input == '+')
		{
			cout << num1 << " added to " << num2 << " equals " << Add(num1, num2) << "\n";
		}
		if (input == '-')
		{
			cout << num1 << " subtracted from " << num2 << " equals " << Subtract(num1, num2) << "\n";
		}
		if (input == '*')
		{
			cout << num1 << " multiplied by " << num2 << " equals " << Multiply(num1, num2) << "\n";
		}
		if (input == '/')
		{
			if (Divide(num1, num2, answer))
			{
				cout << num1 << " divided by " << num2 << " equals " << answer << "\n";
			}
			else
			{
				cout << "You cannot divide by zero.\n";
			}
		}
		if (input == '^')
		{
			cout << num1 << " to the power of " << num2 << " equals " << Pow(num1, num2) << "\n";
		}
		// Option to do another math problem.  If not then the loop ends and the application closes.
		cout << "Care for another math problem? (y/n): ";
		cin >> input;
	}

	return 0;
}

// Functions for formulas.
float Add(float num1, float num2)
{
	return num1 + num2;
}
float Subtract(float num1, float num2)
{
	return num1 - num2;
}
float Multiply(float num1, float num2)
{
	return num1 * num2;
}
bool Divide(float num1, float num2, float &answer)
{
	if (num2 == 0) return false;

	answer = num1 / num2;
	return true;
}
float Pow(float num1, int num2)
{
	if (num2 == 0)
		return 1;
	if (num2 == 1)
		return num1;
	else
		return num1 * Pow(num1, num2 - 1);
}